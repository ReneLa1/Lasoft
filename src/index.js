import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'font-awesome/css/font-awesome.css';
import WebFont from 'webfontloader';
import Routes from './Routes';

WebFont.load({
  google: {
    families: ['Pacifico', 'cursive', 'Roboto', 'Raleway', 'sans-serif']
  }
});

const Root = () => {
  return <Routes />;
};

ReactDOM.render(<Root />, document.getElementById('root'));
