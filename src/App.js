import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/home/Home';
import Header from './components/Header';
import Footer from './components/Footer';
import About from './pages/About/About';
import Contact from './pages/contact/Contact';
import Work from './pages/work/Work';
import Blog from './pages/blog/Blog';
import SingleProjectPage from './pages/work/SingleProjectPage';
import colors from './styles/colors';

function App() {
  return (
    <div style={{ backgroundColor: colors.bgColor }}>
      <Header />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/our_work' exact component={Work} />
        <Route path='/our_work/:id' component={SingleProjectPage} />
        <Route path='/about_us' exact component={About} />
        <Route path='/blog' exact component={Blog} />
        <Route path='/contact_us' exact component={Contact} />
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
