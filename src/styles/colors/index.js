export default {
  primaryColor: '#CD6155',
  // primaryColor: '#76D7C4',
  bgColor: '#343341',
  lightBgColor: '#46475C',
  secondary: '#7DEBB3',
  gray01: '#F2F3F4',
  gray02: '#D6DBDF',
  gray03: '#808B96',
  white: '#FDFEFE',
  black01: '#17202A',
  textColor: '#1C2833',
  textWhiteColor: '#FDFEFE'
  // textColor: '#FDFEFE'
};
