import React from 'react';
import { withStyles, Typography, Paper, Grid, Button } from '@material-ui/core';
import SchoolIcon from '@material-ui/icons/School';
import MapIcon from '@material-ui/icons/Map';
import HeartIcon from '@material-ui/icons/Favorite';
import colors from '../styles/colors';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebook,
  faLinkedin,
  faYoutube,
  faYahoo
} from '@fortawesome/free-brands-svg-icons';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: colors.black01
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 2,
    backgroundColor: 'inherit',
    height: 250,
    borderRadius: 0,
    paddingBottom: 40,
    justifyContent: 'flex-end'
  },
  footerTextStyle: {
    color: colors.white,
    fontSize: 14,
    fontWeight: '400'
  },
  missionDescriptionText: {
    color: colors.white,
    fontSize: 14,
    fontWeight: '300'
  },
  buttonText: {
    color: colors.white,
    borderColor: 'none',
    fontSize: 11,
    boxShadow: 'none',
    textTransform: 'lowerCase',
    '&:hover': {
      backgroundColor: 'inherit',
      color: colors.primaryColor
    }
  },

  rowContainer: {
    display: 'flex',
    flexDirection: 'row'
  }
});

class Footer extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={4} lg={4}>
            <Paper
              elevation={0}
              className={classes.paper}
              style={{ justifyContent: 'flex-end' }}
            >
              <Typography
                variant='subtitle2'
                className={classes.footerTextStyle}
              >
                Made With{' '}
                <HeartIcon
                  style={{ color: 'red', height: 20, width: 20, top: -50 }}
                />{' '}
                in Chengdu, CHINA
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={4} lg={4}>
            <Paper
              elevation={0}
              className={classes.paper}
              style={{
                justifyContent: 'space-between'
              }}
            >
              <div />
              <div
                id='socialContainer'
                className={classes.rowContainer}
                style={{
                  justifyContent: 'center'
                }}
              >
                <FontAwesomeIcon
                  style={{
                    color: 'white',
                    height: 25,
                    width: 25,
                    marginRight: 10
                  }}
                  icon={faFacebook}
                />
                <FontAwesomeIcon
                  style={{
                    color: 'red',
                    height: 25,
                    width: 25,
                    marginRight: 10
                  }}
                  icon={faYoutube}
                />
                <FontAwesomeIcon
                  style={{
                    color: 'blue',
                    height: 25,
                    width: 25,
                    marginRight: 10
                  }}
                  icon={faLinkedin}
                />
                <FontAwesomeIcon
                  style={{ color: 'white', height: 25, width: 25 }}
                  icon={faYahoo}
                />
              </div>

              <div
                className={classes.rowContainer}
                style={{
                  justifyContent: 'space-between'
                }}
              >
                <Typography
                  variant='subtitle2'
                  style={{
                    fontSize: 13
                  }}
                  className={classes.footerTextStyle}
                >
                  &copy; 2019 LaSoft Systems Inc.
                </Typography>
                <Button className={classes.buttonText}>Terms of use</Button>
              </div>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={4} lg={4}>
            <Paper
              elevation={0}
              className={classes.paper}
              style={{
                justifyContent: 'space-between'
              }}
            >
              {/* school location */}
              <div
                className={classes.rowContainer}
                style={{
                  justifyContent: 'flex-end'
                }}
              >
                <Typography
                  variant='subtitle1'
                  style={{ marginTop: 10 }}
                  className={classes.missionDescriptionText}
                >
                  Chengdu Neusoft University <br />
                  No.1 neusoft road, China
                </Typography>
                <SchoolIcon
                  style={{
                    marginLeft: 10,
                    height: 60,
                    width: 60,
                    top: -20,
                    color: colors.gray01
                  }}
                />
              </div>

              {/* office rwanda */}
              <div
                className={classes.rowContainer}
                style={{ justifyContent: 'flex-end' }}
              >
                <Typography
                  variant='subtitle1'
                  style={{ marginTop: 10 }}
                  className={classes.missionDescriptionText}
                >
                  LaSoft Systems Inc <br />
                  Kigali, Rwanda
                </Typography>
                <MapIcon
                  style={{
                    marginLeft: 10,
                    height: 60,
                    width: 60,
                    top: -20,
                    color: colors.gray01
                  }}
                />
              </div>

              {/* privacy and contact */}
              <div
                className={classes.rowContainer}
                style={{
                  justifyContent: 'space-between'
                }}
              >
                <Button className={classes.buttonText}>Privacy Policy</Button>
                <div
                  className={classes.rowContainer}
                  style={{
                    justifyContent: 'center'
                  }}
                >
                  <Button className={classes.buttonText}>15884564750</Button>||
                  <Button className={classes.buttonText}>
                    lasoft@mail.com
                  </Button>
                </div>
              </div>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Footer);
