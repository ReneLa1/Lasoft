import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  withStyles,
  AppBar,
  Toolbar,
  IconButton,
  Menu,
  MenuItem,
  Paper,
  Slide,
  List,
  ListItem,
  ListItemText,
  Button,
  Popover,
  Grid,
  Typography
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowDropUp from '@material-ui/icons/ArrowDropUp';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import colors from '../styles/colors';

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: 'inherit'
  },
  appBarStyle: {
    backgroundColor: 'inherit',
    zIndex: 4
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    // alignItems: 'flex-end',
    paddingTop: 40,
    height: 200,
    backgroundColor: 'inherit',
    borderRadius: 0
  },
  gridContainer: {
    display: 'flex',
    backgroundColor: 'inherit',
    marginLeft: 100,
    marginRight: 100
  },
  button: {
    borderColor: colors.secondary,
    backgroundColor: 'inherit',
    color: colors.textWhiteColor,
    fontSize: 15,
    fontWeight: '600',
    '&:hover': {
      borderColor: colors.secondary,
      color: colors.textColor,
      backgroundColor: colors.secondary
    }
  },
  grow: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-end',
    [theme.breakpoints.up('sm', 'md')]: {
      display: 'none'
    }
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('xs', 'sm')]: {
      display: 'flex',
      color: colors.textWhiteColor
    }
  },
  popover: {
    // pointerEvents: 'none',
    padding: 0,
    position: 'absolute',
    top: 60,
    height: 260,
    backgroundColor: colors.bgColor
  },
  popoverPaper: {
    display: 'flex',
    width: '100%',
    marginTop: -50,
    backgroundColor: colors.bgColor,
    borderRadius: 0
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
      flexGrow: 1,
      flex: 1,
      justifyContent: 'flex-end'
    }
  },
  navLinks: {
    display: 'flex',
    flexGrow: 1,
    flex: 1.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingTop: 15
  },
  navStyle: {
    marginLeft: 10,
    marginRight: 10,
    color: colors.textWhiteColor,
    textDecoration: 'none',
    fontFamily: 'Raleway',
    fontSize: 13,
    fontWeight: 'bold',
    paddingRight: 10,
    paddingLeft: 10,
    '&:hover': {
      color: colors.primaryColor
    }
  },
  navAction: {
    display: 'flex',
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  menuText: {
    fontFamily: 'Roboto',
    color: colors.textColor,
    fontWeight: '600',
    fontSize: 14,
    '&:hover': {
      color: colors.primaryColor
    }
  }
});

class Header extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    slideServiceMenuAnchorEl: null,
    slideExpertiseMenuAnchorEl: null
  };

  handleSlideServiceMenuOpen = event => {
    this.setState({ slideServiceMenuAnchorEl: event.currentTarget });
  };
  handleSlideServiceMenuClose = () => {
    this.setState({ slideServiceMenuAnchorEl: null });
  };

  handleSlideExpertiseMenuOpen = event => {
    this.setState({ slideExpertiseMenuAnchorEl: event.currentTarget });
  };
  handleSlideExpertiseMenuClose = () => {
    this.setState({ slideExpertiseMenuAnchorEl: null });
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  render() {
    const {
      anchorEl,
      mobileMoreAnchorEl,
      slideServiceMenuAnchorEl,
      slideExpertiseMenuAnchorEl
    } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
    const isServiceMenuOpen = Boolean(slideServiceMenuAnchorEl);
    const isExpertiseMenuOpen = Boolean(slideExpertiseMenuAnchorEl);

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={this.handleMenuClose}>My account</MenuItem>
      </Menu>
    );

    const renderOnHoverServicesMenu = (
      <Popover
        elevation={0}
        id='mouse-over-popover'
        className={classes.popover}
        classes={{
          paper: classes.popoverPaper
        }}
        open={isServiceMenuOpen}
        anchorEl={slideServiceMenuAnchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        onExit={this.handleSlideServiceMenuClose}
        disableRestoreFocus
      >
        <Grid
          onMouseLeave={this.handleSlideServiceMenuClose}
          container
          className={classes.gridContainer}
          spacing={0}
        >
          <Grid onMouseOver={this.handleSlideServiceMenuClose} item xs={4}>
            <Paper elevation={0} className={classes.paper} />
          </Grid>
          <Grid item xs={4}>
            <Paper elevation={0} className={classes.paper}>
              <Typography
                variant='button'
                gutterBottom
                className={classes.navStyle}
                style={{ color: colors.gray03, marginBottom: 25 }}
              >
                DESIGN & DEVELOPMENT
              </Typography>
              <Typography
                variant='button'
                component={Link}
                to={'/about_us'}
                gutterBottom
                onClick={this.handleSlideServiceMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                PRODUCT DESIGN
              </Typography>
              <Typography
                component={Link}
                to={'/our_work'}
                variant='button'
                onClick={this.handleSlideServiceMenuClose}
                gutterBottom
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                MVP DEVELOPMENT
              </Typography>
              <Typography
                variant='button'
                component={Link}
                to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideServiceMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                CONTINUOUS PRODUCT DEVELOPMENT
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper elevation={0} className={classes.paper}>
              <Typography
                variant='button'
                gutterBottom
                className={classes.navStyle}
                style={{ color: colors.gray03, marginBottom: 25 }}
              >
                CONSULTING WORKSHOPS
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideServiceMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                MOBILE STRATEGY WORKSHOP
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideServiceMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                PRODUCT EVALUATION WORKSHOP
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </Popover>
    );

    const renderOnHoverExpertiseMenu = (
      <Popover
        elevation={0}
        marginThreshold={16}
        id='mouse-over-popover'
        className={classes.popover}
        classes={{
          paper: classes.popoverPaper
        }}
        open={isExpertiseMenuOpen}
        anchorEl={slideExpertiseMenuAnchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left'
        }}
        disableRestoreFocus
      >
        <Grid
          onMouseLeave={this.handleSlideExpertiseMenuClose}
          container
          className={classes.gridContainer}
          spacing={0}
        >
          <Grid onMouseOver={this.handleSlideExpertiseMenuClose} item xs={3}>
            <Paper elevation={0} className={classes.paper} />
          </Grid>
          <Grid item xs={3}>
            <Paper elevation={0} className={classes.paper}>
              <Typography
                variant='button'
                className={classes.navStyle}
                style={{ color: colors.gray03, marginBottom: 25 }}
              >
                INDUSTRIES
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                HEALTH CARE
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                ON-DEMAND ECONOMY
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                FINANCE
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                MUSIC
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                FITNESS AND SPORTS
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={3}>
            <Paper elevation={0} className={classes.paper}>
              <Typography
                variant='button'
                gutterBottom
                className={classes.navStyle}
                style={{ color: colors.gray03, marginBottom: 25 }}
              >
                DESIGN
              </Typography>
              <Typography
                variant='button'
                gutterBottom
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                USER EXPERIENCE
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                INTERFACE DESIGN
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                CONVERSATIONAL UI
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={3}>
            <Paper elevation={0} className={classes.paper}>
              <Typography
                variant='button'
                gutterBottom
                className={classes.navStyle}
                style={{ color: colors.gray03, marginBottom: 25 }}
              >
                TECHNOLOGIES
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                ANDROID
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                REACT NATIVE
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                FLUTTER
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                REACT JS
              </Typography>
              <Typography
                variant='button'
                // component={Link}
                // to={'/contact_us'}
                gutterBottom
                onClick={this.handleSlideExpertiseMenuClose}
                className={classes.navStyle}
                style={{ marginBottom: 15 }}
              >
                BACKENDS
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </Popover>
    );

    const renderMobileMenu = (
      <Slide direction='down' in={isMobileMenuOpen} mountOnEnter unmountOnExit>
        <Paper elevation={0} className={classes.paper}>
          <List component='nav'>
            <ListItem button>
              <ListItemText
                primary='SERVICES'
                classes={{ primary: classes.menuText }}
              />
            </ListItem>
            <ListItem>
              <ListItemText
                primary='EXPERTISE'
                classes={{ primary: classes.menuText }}
              />
            </ListItem>
            <ListItem button>
              <ListItemText
                primary='WORK'
                classes={{ primary: classes.menuText }}
              />
            </ListItem>

            <ListItem button>
              <ListItemText
                primary='ABOUT US'
                classes={{ primary: classes.menuText }}
              />
            </ListItem>
            <ListItem button>
              <ListItemText
                primary='BLOG'
                classes={{ primary: classes.menuText }}
              />
            </ListItem>
          </List>
        </Paper>
      </Slide>
    );

    return (
      <div className={classes.root}>
        <AppBar
          position='fixed'
          elevation={isMobileMenuOpen ? 2 : 0}
          className={classes.appBarStyle}
        >
          <Toolbar>
            <Typography
              className={classes.title}
              component={Link}
              to={'/'}
              variant='h6'
              // color='inherit'
              noWrap
              id='logoText'
            >
              LaSoft Systems
            </Typography>
            <div className={classes.grow} />

            <div className={classes.sectionDesktop}>
              <div className={classes.navLinks}>
                <Typography
                  onMouseEnter={this.handleSlideServiceMenuOpen}
                  variant='subtitle1'
                  gutterBottom
                  className={classes.navStyle}
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    // top: -20,
                    color: isServiceMenuOpen ? colors.primaryColor : ''
                  }}
                >
                  Services
                  {isServiceMenuOpen ? (
                    <ArrowDropUp
                      style={{ fontSize: 23, color: colors.primaryColor }}
                    />
                  ) : (
                    <ArrowDropDown style={{ fontSize: 23 }} />
                  )}
                </Typography>
                <Typography
                  onMouseEnter={this.handleSlideExpertiseMenuOpen}
                  variant='subtitle1'
                  gutterBottom
                  className={classes.navStyle}
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    color: isExpertiseMenuOpen ? colors.primaryColor : ''
                  }}
                >
                  Expertise
                  {isExpertiseMenuOpen ? (
                    <ArrowDropUp
                      style={{ fontSize: 23, color: colors.primaryColor }}
                    />
                  ) : (
                    <ArrowDropDown style={{ fontSize: 23 }} />
                  )}
                </Typography>
                <Typography
                  variant='subtitle1'
                  component={Link}
                  to={'/our_work'}
                  gutterBottom
                  className={classes.navStyle}
                >
                  Work
                </Typography>
                <Typography
                  variant='subtitle1'
                  component={Link}
                  to={'/about_us'}
                  gutterBottom
                  className={classes.navStyle}
                >
                  About Us
                </Typography>
                <Typography
                  variant='subtitle1'
                  component={Link}
                  to={'/blog'}
                  gutterBottom
                  className={classes.navStyle}
                >
                  Blog
                </Typography>
              </div>
              <div className={classes.navAction}>
                <Button
                  variant='outlined'
                  color='primary'
                  component={Link}
                  to={'/contact_us'}
                  className={classes.button}
                >
                  Contact Us
                </Button>
              </div>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                aria-haspopup='true'
                onClick={this.handleMobileMenuOpen}
                style={{ color: colors.black01 }}
              >
                <MenuIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        {renderMenu}
        {renderOnHoverServicesMenu}
        {renderOnHoverExpertiseMenu}
        {renderMobileMenu}
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
