import React from 'react';
import {
  withStyles,
  Typography,
  Paper,
  Grid,
  Card,
  CardContent,
  Avatar
  // Button
  // TextField,
  // Button
} from '@material-ui/core';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';
const coreImg = require('../../images/star.jpg');
const davy = require('../../images/kevin.jpg');
const rene = require('../../images/rene1.jpg');
const cy = require('../../images/cy.jpeg');

const teamMembers = [
  {
    name: 'Unix Kevin',
    role: 'Head of User Experience',
    img: davy
  },
  {
    name: 'Rene La',
    role: 'Founder, CEO & Head of Engineering',
    img: rene
  },
  {
    name: 'Patrick Cy',
    role: 'Software Engineer',
    img: cy
  }
];

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: 70,
    flexGrow: 1
  },
  gridContainer: {
    backgroundImage: `url(${coreImg})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    padding: 30,
    borderRadius: 0
  },
  serviceRowStyle: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    padding: 10,
    height: 400,
    borderRadius: 0,
    backgroundColor: colors.gray02,
    [theme.breakpoints.down('xs', 'sm', 'md')]: {
      height: 400
    }
  },
  headerTwoStyle: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: '700',
    marginTop: 15
  },
  descriptionTextStyle: {
    color: colors.textColor,
    fontWeight: '300',
    margin: 30
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 4,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.text.secondary,
    borderRadius: 0
  },
  missionText: {
    color: colors.textColor,
    fontSize: constants.headerTextTwo,
    fontWeight: '900',
    marginTop: 30,
    [theme.breakpoints.down('xs', 'sm')]: {
      marginTop: 10
    }
  },
  missionDescriptionText: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: '300',
    marginTop: 30,
    [theme.breakpoints.down('xs', 'sm')]: {
      marginTop: 10
    }
  },
  buttonOutLine: {
    borderColor: colors.primaryColor,
    backgroundColor: colors.primaryColor,
    color: colors.white,
    fontSize: 15,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 20,
    width: 350,
    marginBottom: 30,
    '&:hover': {
      borderColor: colors.primaryColor,
      color: colors.primaryColor,
      backgroundColor: colors.white
    }
  },

  bigAvatar: {
    width: 150,
    height: 150
  },
  profileName: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10,
    padding: 0
  },
  profileInfo: {
    color: colors.textColor,
    fontSize: 16,
    fontWeight: '500',
    marginTop: 5,
    marginBottom: 10,
    padding: 0
  }
});

const About = ({ classes }) => {
  const memberInfoCard = (name, role, img) => (
    <Card elevation={0} className={classes.paper}>
      <Avatar alt='Rene La' src={img} className={classes.bigAvatar} />
      <CardContent>
        <Typography gutterBottom variant='h6' className={classes.profileName}>
          {name}
        </Typography>
        <Typography
          variant='subtitle1'
          gutterBottom
          className={classes.profileInfo}
        >
          {role}
        </Typography>
      </CardContent>
    </Card>
  );
  return (
    <div className={classes.root}>
      {/* team members */}
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper elevation={0} className={classes.paper}>
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
            >
              Our Great Team
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.missionDescriptionText}
            >
              Meet Our Skilled & Hardworking Team
            </Typography>
          </Paper>
        </Grid>
        {teamMembers.map(member => {
          return (
            <Grid item key={member.name} xs={12} sm={12} md={4} lg={4}>
              {memberInfoCard(member.name, member.role, member.img)}
            </Grid>
          );
        })}
      </Grid>

      {/* philosophy */}
      <Grid container className={classes.gridContainer} spacing={32}>
        <Grid item xs={12}>
          <Paper
            elevation={0}
            className={classes.paper}
            style={{ backgroundColor: 'transparent' }}
          >
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
              style={{ color: colors.white }}
            >
              Our Mission and Philosophy
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.missionDescriptionText}
              style={{ color: colors.white }}
            >
              LaSoft Systems' mission is to clear the path to success <br />
              for your business with effective mobile strategies, beautiful
              design, and world-class development.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Product Philosophy
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Your mobile app is more than a project for us. We focus on your
              business goals and dive deep to understand your users’ behavioral
              patterns. There's no guesswork, only sharp design decisions and
              flawless functionality. That is why our product approach delivers
              mobile apps that win.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              We're a "Big" Small Agency
            </Typography>

            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Back in the day, you'd open the door to your local bakery, and the
              baker would have your favorite bread fresh and ready to go. She
              might have had hundreds of customers, but she would always make
              sure you get that extra care and attention. That was a “big” small
              bakery. We bring that attitude back to life!
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Design is the Soul
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Great design is a universal language, communicating how something
              looks and how it works. It is through design that ideas become
              visually and aesthetically tangible. Your users’ delight dwells in
              beautiful design, and it is with our design-oriented approach that
              we can help you unleash it.
            </Typography>
          </Paper>
        </Grid>
      </Grid>

      {/* core values */}
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper elevation={0} className={classes.paper}>
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
            >
              Our Core Values
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.missionDescriptionText}
            >
              The four fundamentals LaSoft is all about,
              <br /> that we all believe in.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Product Approach
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Products do have a soul. We wrap our processes and expertise into
              a framework for creating outstanding products.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Aesthetics and Usability
            </Typography>

            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Combining wow-inspiring user interfaces, experiences and mobile
              product strategy is our secret sauce for success.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Mutual Growth
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Our performance is based on how successful our clients become with
              the apps we build. We do our best to help you achieve success!
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              We Speak Your Language
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Whether you are tech-savvy or not, we will make our process one
              hundred percent understandable for you.
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(About);
