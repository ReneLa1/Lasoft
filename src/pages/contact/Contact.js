import React from 'react';
import {
  withStyles,
  Typography,
  Paper,
  Grid,
  TextField,
  Button
} from '@material-ui/core';

import colors from '../../styles/colors';
import * as constants from '../../styles/constants';

const styles = theme => ({
  root: {
    display: 'flex',
    paddingTop: 70,
    flexGrow: 1
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 4,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.text.secondary
  },
  missionText: {
    color: colors.textColor,
    fontSize: constants.headerTextTwo,
    fontWeight: '900',
    marginLeft: 10
  },
  missionDescriptionText: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: '300',
    marginLeft: 10
  },
  container: {
    display: 'flex',
    // flexWrap: 'wrap',
    width: '100%',
    flexDirection: 'column',
    // backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 500,
    [theme.breakpoints.down('xs', 'sm')]: {
      width: 400
    }
  },
  buttonOutLine: {
    borderColor: colors.primaryColor,
    backgroundColor: colors.primaryColor,
    color: colors.white,
    fontSize: 15,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 20,
    width: 350,
    marginBottom: 30,
    '&:hover': {
      borderColor: colors.primaryColor,
      color: colors.primaryColor,
      backgroundColor: colors.white
    }
  }
});

// const handleChange = name => event => {
//   this.setState({
//     [name]: event.target.value
//   });
// };

const Contact = ({ classes }) => {
  return (
    <div className={classes.root}>
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper elevation={0} className={classes.paper}>
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
            >
              Tell Us about your project
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.missionDescriptionText}
            >
              Let's see how a Beautiful, Smart APP can grow your Business
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={0} className={classes.paper}>
            <form className={classes.container} noValidate autoComplete='off'>
              <TextField
                id='name'
                required
                label='Name'
                placeholder='your name'
                className={classes.textField}
                margin='normal'
                variant='outlined'
              />
              <TextField
                id='email'
                label='Email'
                required
                className={classes.textField}
                type='email'
                name='email'
                autoComplete='email'
                margin='normal'
                variant='outlined'
              />
              <TextField
                id='company'
                label='Company name'
                placeholder='your company name'
                className={classes.textField}
                margin='normal'
                variant='outlined'
              />
              <TextField
                id='phone'
                label='Phone number'
                placeholder='your your phone number'
                className={classes.textField}
                margin='normal'
                variant='outlined'
              />
              <TextField
                id='textarea'
                required
                label='tell us about your project'
                multiline
                rows={8}
                // value={this.state.multiline}
                // onChange={this.handleChange('multiline')}
                className={classes.textField}
                margin='normal'
                variant='outlined'
              />
              <Button
                variant='outlined'
                color='primary'
                className={classes.buttonOutLine}
              >
                Submit
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Contact);
