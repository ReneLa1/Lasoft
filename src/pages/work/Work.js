import React from 'react';
import { Link } from 'react-router-dom';
import { withStyles, Paper, Grid, Typography, Button } from '@material-ui/core';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';

const app1 = require('../../images/projects/app1.jpg');
const app2 = require('../../images/projects/app2.jpg');
const app3 = require('../../images/projects/app3.jpg');
const case1 = require('../../images/projects/case1.jpg');
const case2 = require('../../images/projects/case2.jpg');
const case3 = require('../../images/projects/case3.png');
// #000F14
const workList = [
  {
    id: 1,
    name: 'NICKSON',
    title: 'ON-DEMAND FURNITURE RENTAL PLATFORM',
    description:
      'An on-demand furniture rental platform, Nickson Living gives people the affordable opportunity to live in a custom-designed, fully-furnished apartment of their dreams.',
    caption: '',
    client: 'Nickson Inc.',
    link: 'nicksonliving.com',
    city: 'Dallas, Texas',
    color: '#020914',
    coverImg: app1,
    category: 'project',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },

  {
    id: 2,
    name: 'Q.Care',
    title: 'ON-DEMAND BOOKING AND GUIDE',
    description:
      'A trip to Las Vegas is an out of life experience. Plan a trip that includes fine dining in a celebrity chef owned 5 star restaurant, thrills like the zip line, and special attractions.',
    caption: 'Vegaster raised $1.1 Million in 2015.',
    client: 'Facebook Inc',
    link: 'www.facebook.com',
    city: 'San Franscisco',
    color: '#797D7F',
    coverImg: case2,
    category: 'case_study',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 3,
    name: 'The Sports Hub',
    title: 'PLATFORM FOR HOCKEY PLAYERS',
    description:
      'Supporting amateur athletes, The Sports Aux is a platform for hockey players that bridges the gap between rink and stands.',
    caption: '',
    client: 'Google Inc',
    link: 'www.google.com',
    city: 'Palo Alto, CA',
    color: '#000000',
    coverImg: app2,
    category: 'project',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 4,
    name: 'Q.Care',
    title: 'ON-DEMAND PEDIATRIC HELP',
    description:
      'Through PediaQ’s platform, Q.care, hospitals can use a “White Label” smartphone app to offer nurse triage and house calls. The main goal is to reduce unnecessary emergency room visits.',
    caption: 'Q.Care raised $4.5 million in November 2016',
    client: 'Adobe ',
    link: 'www.adobe.com',
    city: 'New york',
    color: '#5DADE2',
    coverImg: case1,
    category: 'case_study',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },

  {
    id: 5,
    name: 'Swiftmile',
    title: 'BIKE SHARING SYSTEM',
    description:
      'An on-demand eBike sharing system that helps get people out of their cars and where they need to go. The award-winning system is deployed at some of the most innovative companies in the world.',
    caption:
      'Raised $750,000 led by Verizon Ventures. being adopted by Xerox PARC, and more.',
    client: 'HviewTech',
    link: 'www.hviewTech.com',
    city: 'Kigali, Rwanda',
    color: '#273746',
    coverImg: app3,
    category: 'project',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 6,
    name: 'Q.Care',
    title: 'ON-DEMAND BOOKING AND GUIDE',
    description:
      'A trip to Las Vegas is an out of life experience. Plan a trip that includes fine dining in a celebrity chef owned 5 star restaurant, thrills like the zip line, and special attractions.',
    caption: 'Vegaster raised $1.1 Million in 2015.',
    client: 'Sparbee',
    link: 'www.sparbee.com',
    city: 'Chengdu, China',
    color: '#A3E4D7',
    coverImg: case3,
    category: 'case_study',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  }
];

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: 64,
    flexGrow: 1
  },
  // gridContainer: {
  //   backgroundColor: colors.white,
  //   borderRadius: 5,
  //   marginLeft: 60,
  //   marginRight: 60,
  //   [theme.breakpoints.down('xs', 'sm')]: {
  //     marginLeft: 0,
  //     marginRight: 0,
  //     top: 0,
  //     borderRadius: 0
  //   }
  // },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing.unit * 4,
    height: 386,
    borderRadius: 0,
    color: theme.palette.text.secondary
  },
  paperImage: {
    height: 450,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    borderRadius: 0
  },
  headerTwoStyle: {
    color: colors.textColor,
    fontSize: 18,
    fontWeight: '700',
    marginTop: 15,
    padding: 10,
    width: 300,
    backgroundColor: colors.gray01
  },
  descriptionTextStyle: {
    color: colors.white,
    fontWeight: '300',
    marginTop: 30,
    marginBottom: 30
  },
  missionText: {
    color: colors.white,
    fontSize: constants.headerTextTwo,
    fontWeight: '900',
    marginLeft: 10
  },
  buttonOutLine: {
    borderColor: colors.primaryColor,
    borderRadius: 0,
    color: colors.primaryColor,
    backgroundColor: 'inherit',
    fontSize: 15,
    fontWeight: '600',
    width: 250,
    '&:hover': {
      borderColor: colors.primaryColor,
      backgroundColor: colors.primaryColor,
      color: colors.white
    }
  },
  moreProj: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 4,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.text.secondary,
    borderRadius: 0
  }
});

const Work = ({ classes }) => {
  const ProjectInfo = proj => (
    <Paper
      elevation={0}
      style={{ backgroundColor: proj.color }}
      className={classes.paper}
    >
      <Typography variant='h4' gutterBottom className={classes.missionText}>
        {proj.name}
      </Typography>
      <Typography variant='h5' gutterBottom className={classes.headerTwoStyle}>
        {proj.title}
      </Typography>
      <Typography
        variant='subtitle1'
        gutterBottom
        className={classes.descriptionTextStyle}
      >
        {proj.description}
      </Typography>
      <Typography
        variant='subtitle1'
        gutterBottom
        style={{ marginTop: 5 }}
        className={classes.descriptionTextStyle}
      >
        {proj.caption}
      </Typography>
      {proj.category === 'project' ? (
        <Button
          component={Link}
          to={{
            pathname: `our_work/${proj.name.toLowerCase()}`,
            // search: "?sort=name",
            // hash: "#the-hash",
            state: { singleProj: proj }
          }}
          // to={`our_work/${proj.name.toLowerCase()}`}
          variant='outlined'
          color='primary'
          className={classes.buttonOutLine}
        >
          View Project
        </Button>
      ) : (
        <Button
          component={Link}
          to={'/'}
          variant='outlined'
          color='primary'
          className={classes.buttonOutLine}
        >
          See Case Study
        </Button>
      )}
    </Paper>
  );

  return (
    <div className={classes.root}>
      {workList.map((wrk, i) => {
        if (i % 2) {
          return (
            <Grid
              container
              spacing={0}
              key={wrk.id}
              className={classes.gridContainer}
            >
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                {ProjectInfo(wrk)}
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <Paper
                  elevation={0}
                  className={classes.paperImage}
                  style={{ backgroundImage: `url(${wrk.coverImg})` }}
                />
              </Grid>
            </Grid>
          );
        } else {
          return (
            <Grid
              container
              spacing={0}
              key={wrk.id}
              className={classes.gridContainer}
            >
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <Paper
                  elevation={0}
                  className={classes.paperImage}
                  style={{ backgroundImage: `url(${wrk.coverImg})` }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                {ProjectInfo(wrk)}
              </Grid>
            </Grid>
          );
        }
      })}

      {/* more projects */}
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper elevation={0} className={classes.moreProj}>
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
              style={{ color: colors.black01 }}
            >
              More of Our Work
            </Typography>
          </Paper>
        </Grid>
        {/* <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Product Approach
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Products do have a soul. We wrap our processes and expertise into
              a framework for creating outstanding products.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Aesthetics and Usability
            </Typography>

            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Combining wow-inspiring user interfaces, experiences and mobile
              product strategy is our secret sauce for success.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              Mutual Growth
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Our performance is based on how successful our clients become with
              the apps we build. We do our best to help you achieve success!
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <Paper elevation={0} className={classes.serviceRowStyle}>
            <Typography
              variant='h5'
              gutterBottom
              className={classes.headerTwoStyle}
            >
              We Speak Your Language
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              className={classes.descriptionTextStyle}
            >
              Whether you are tech-savvy or not, we will make our process one
              hundred percent understandable for you.
            </Typography>
          </Paper>
        </Grid> */}
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Work);
