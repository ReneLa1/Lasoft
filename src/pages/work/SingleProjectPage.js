import React from 'react';
import {
  withStyles,
  Typography,
  Grid,
  Paper,
  Divider,
  Hidden,
  Card,
  CardMedia,
  Drawer
} from '@material-ui/core';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';

const styles = theme => ({
  root: {
    display: 'flex',
    flexGrow: 1
  },
  paper: {
    height: 400,
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  paperImage: {
    height: 300,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    borderRadius: 0,
    marginBottom: 30
  },
  drawer: {
    // width: '100%'
    // flexShrink: 0
  },
  drawerPaper: {
    // width: '100%'
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3
  },
  card: {
    borderRadius: 0,
    marginBottom: 30
  },
  media: {
    height: 500,
    [theme.breakpoints.up('xs')]: {
      height: 250
    },
    [theme.breakpoints.up('sm')]: {
      height: 350
    }
  },
  serviceRowStyle: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'inherit',
    paddingLeft: 10,
    paddingRight: 10
  },
  headerTwoStyle: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: '700',
    marginBottom: 15
  },
  descriptionTextStyle: {
    color: colors.textColor,
    // fontWeight: '300',
    marginTop: 20,
    marginBottom: 15
  }
});

const SingleProjectPage = props => {
  const { classes } = props;
  const { singleProj } = props.location.state;
  console.log(singleProj);
  const { name, description, caption, client, city, link } = singleProj;
  return (
    <div className={classes.root}>
      <Grid container spacing={0}>
        <Grid
          style={{ height: '100vh', overflow: 'auto' }}
          item
          xs={12}
          sm={12}
          md={8}
          lg={8}
        >
          {/* <Hidden only={['xs', 'sm', 'md']}>
            <div className={classes.toolbar} />
          </Hidden> */}
          <Drawer
            className={classes.drawer}
            variant='permanent'
            ModalProps={{
              style: {
                position: 'absolute'
              }
            }}
            PaperProps={{
              style: {
                display: 'flex',
                flexDirection: 'column',
                flex: 1,
                position: 'relative',
                paddingTop: 30,
                paddingLeft: 30,
                paddingRight: 30,
                zIndex: -4,
                borderColor: 'none',
                borderWidth: 0,
                // height: 300,
                backgroundColor: colors.white
              }
            }}
            classes={{
              paper: classes.drawerPaper
            }}
            anchor='left'
          >
            <div className={classes.toolbar} />
            {singleProj.photoShot.map(photo => {
              return (
                <Card elevation={0} key={photo.id} className={classes.card}>
                  <CardMedia className={classes.media} image={photo.path} />
                </Card>
              );
            })}
          </Drawer>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <main className={classes.content}>
            <Hidden only={['xs', 'sm', 'md']}>
              {/* <div className={classes.toolbar} /> */}
              <div className={classes.toolbar} />
            </Hidden>
            <Paper elevation={0} className={classes.serviceRowStyle}>
              <Typography
                variant='h5'
                gutterBottom
                className={classes.headerTwoStyle}
              >
                {name}
              </Typography>
              <Typography
                variant='subtitle1'
                gutterBottom
                className={classes.descriptionTextStyle}
              >
                {description}
              </Typography>
              <Typography
                variant='subtitle1'
                gutterBottom
                className={classes.descriptionTextStyle}
              >
                {caption}
              </Typography>
              <Divider />
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  marginTop: 20,
                  marginBottom: 20
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                  }}
                >
                  <div style={{ flex: 0.5 }}>
                    <Typography
                      variant='body1'
                      style={{
                        fontSize: 16,
                        color: colors.textColor,
                        fontWeight: '600'
                      }}
                      gutterBottom
                    >
                      Client
                    </Typography>
                  </div>

                  <div style={{ flex: 1.5 }}>
                    <Typography
                      variant='subtitle1'
                      style={{ flex: 2.5 }}
                      gutterBottom
                    >
                      {client}
                    </Typography>
                  </div>
                </div>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                  }}
                >
                  <div style={{ flex: 0.5 }}>
                    <Typography
                      variant='body1'
                      style={{
                        fontSize: 16,
                        color: colors.textColor,
                        fontWeight: '600'
                      }}
                      gutterBottom
                    >
                      Link
                    </Typography>
                  </div>

                  <div style={{ flex: 1.5 }}>
                    <Typography
                      variant='subtitle1'
                      style={{ flex: 2.5 }}
                      gutterBottom
                    >
                      {link}
                    </Typography>
                  </div>
                </div>

                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                  }}
                >
                  <div style={{ flex: 0.5 }}>
                    <Typography
                      variant='body1'
                      style={{
                        fontSize: 16,
                        color: colors.textColor,
                        fontWeight: '600'
                      }}
                      gutterBottom
                    >
                      Location
                    </Typography>
                  </div>

                  <div style={{ flex: 1.5 }}>
                    <Typography
                      variant='subtitle1'
                      style={{ flex: 2.5 }}
                      gutterBottom
                    >
                      {city}
                    </Typography>
                  </div>
                </div>
              </div>
              <Divider />
              {/* <Button
                variant='outlined'
                color='primary'
                className={classes.buttonOutLine}
              >
                Learn More
              </Button> */}
            </Paper>
          </main>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(SingleProjectPage);
