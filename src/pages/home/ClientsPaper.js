import React from 'react';
import {
  withStyles,
  MobileStepper,
  IconButton,
  Paper,
  Typography,
  Avatar
} from '@material-ui/core';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';

const client = require('../../images/rene1.jpg');
const client1 = require('../../images/sandy1.jpg');
const client2 = require('../../images/kevin.jpg');
const client3 = require('../../images/flora.jpg');

const clientSteps = [
  {
    name: 'Rene La',
    work: 'LaSoft Systems Inc',
    location: 'Chengdu, CHINA',
    imgPath: client,
    descript:
      'Grow a scalable product, alongside full technological support We’ll help you to validate & understand your product’s value approach, while efficiently taking full advantage of the MVP model– from hypothesis, to market, & beyond'
  },
  {
    name: 'Sandrine',
    work: 'Facebook Inc',
    location: 'San Franscisco, CA ',
    imgPath: client1,
    descript:
      'Grow a scalable product, alongside full technological support We’ll help you to validate & understand your product’s value approach, while efficiently taking full advantage of the MVP model– from hypothesis, to market, & beyond'
  },
  {
    name: 'UniX Kevin',
    work: 'Sun MicroSystems ',
    location: 'Washington, DC',
    imgPath: client2,
    descript:
      'Grow a scalable product, alongside full technological support We’ll help you to validate & understand your product’s value approach, while efficiently taking full advantage of the MVP model– from hypothesis, to market, & beyond'
  },
  {
    name: 'Flora Monroe',
    work: 'Postivo LGT',
    location: 'Kigali, Rwanda',
    imgPath: client3,
    descript:
      'Grow a scalable product, alongside full technological support We’ll help you to validate & understand your product’s value approach, while efficiently taking full advantage of the MVP model– from hypothesis, to market, & beyond'
  }
];

const styles = theme => ({
  root: {
    display: 'flex',
    height: 350,
    flexDirection: 'column',
    backgroundColor: colors.gray01
  },
  paperStyle: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: colors.gray01,
    height: 325,
    justifyContent: 'space-between'
  },
  bigAvatar: {
    width: 70,
    height: 70
  },
  profileName: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: 'bold',
    margin: 0,
    padding: 0
  },
  profileInfo: {
    color: colors.textColor,
    fontSize: 16,
    fontWeight: '500',
    margin: 0,
    padding: 0
  },
  iconButton: {
    backgroundColor: 'inherit',
    '&:hover': {
      borderColor: 'inherit',
      backgroundColor: 'inherit',
      color: colors.black01
    }
  },
  mobileStepper: {
    width: '100%',
    margin: 0
  }
});

class ClientsPaper extends React.Component {
  state = {
    activeStep: 0
  };

  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1
    }));
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1
    }));
  };

  handleStepChange = activeStep => {
    this.setState({ activeStep });
  };

  render() {
    const { classes, theme } = this.props;
    const { activeStep } = this.state;
    const maxSteps = clientSteps.length;

    return (
      <div className={classes.root}>
        <Paper square elevation={0} className={classes.paperStyle}>
          <IconButton
            disableRipple
            className={classes.iconButton}
            onClick={this.handleBack}
            disabled={activeStep === 0}
          >
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
          </IconButton>
          <div
            style={{
              flex: 1,
              flexDirection: 'column',
              padding: 40
            }}
          >
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <Avatar
                alt='Rene La'
                src={clientSteps[activeStep].imgPath}
                className={classes.bigAvatar}
              />
              <div
                id='personalInfo'
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  margin: 10,
                  paddingLeft: 15
                }}
              >
                <Typography variant='title' className={classes.profileName}>
                  {clientSteps[activeStep].name}
                </Typography>
                <Typography variant='subtitle2' className={classes.profileInfo}>
                  {clientSteps[activeStep].work}
                </Typography>
                <Typography variant='subtitle2' className={classes.profileInfo}>
                  {clientSteps[activeStep].location}
                </Typography>
              </div>
            </div>

            <div id='descriptContainer' style={{ flex: 1, marginTop: 10 }}>
              <Typography variant='subtitle2' className={classes.profileInfo}>
                {clientSteps[activeStep].descript}
              </Typography>
            </div>
          </div>

          <IconButton
            disableRipple
            onClick={this.handleNext}
            className={classes.iconButton}
            disabled={activeStep === maxSteps - 1}
          >
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </IconButton>
        </Paper>
        <MobileStepper
          steps={maxSteps}
          position='static'
          activeStep={activeStep}
          className={classes.mobileStepper}
          style={{
            backgroundColor: colors.gray01,
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            paddingTop: 20,
            paddingBottom: 20
          }}
        />
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(ClientsPaper);
