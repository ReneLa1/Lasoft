import React from 'react';
import { Paper, withStyles } from '@material-ui/core';
import colors from '../../styles/colors';

const styles = theme => ({
  paperGrid: {
    display: 'flex',
    flexDirection: 'column',
    height: 350,
    borderRadius: 0,
    justifyContent: 'center',
    backgroundColor: colors.white
  }
});

const ProjectsPaper = ({ classes }) => {
  return <Paper elevation={0} className={classes.paperGrid} />;
};

export default withStyles(styles)(ProjectsPaper);
