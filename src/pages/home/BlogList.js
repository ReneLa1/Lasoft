import React from 'react';
import {
  withStyles,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  Grid
} from '@material-ui/core';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';

const prod1 = require('../../images/airbnb.jpg');
const prod2 = require('../../images/ipad.jpg');
const prod3 = require('../../images/app.jpg');

const blogList = [
  {
    id: 1,
    img: prod1,
    title: "2018'S STRATEGIC MOBILE APP RESOURCES",
    descript: ''
  },
  { id: 2, img: prod2, title: 'EMR: Biometric and BlockChain', descript: '' },
  {
    id: 3,
    img: prod3,
    title: 'Does your Mobile Medical APP need FDA approval',
    descript: ''
  }
];

const styles = theme => ({
  card: {
    width: '100%',
    borderRadius: 0,
    borderColor: 'none',
    backgroundColor: colors.white
  },
  blogContainer: {
    backgroundColor: colors.white,
    borderRadius: 5,
    marginLeft: 60,
    marginRight: 60,
    [theme.breakpoints.down('xs', 'sm')]: {
      marginLeft: 0,
      marginRight: 0,
      top: 0,
      borderRadius: 0
    }
  },
  buttonText: {
    color: colors.textColor,
    borderColor: 'none',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: 'inherit',
      color: colors.primaryColor
    }
  },
  headerTwoStyle: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: '700',
    marginTop: 20,
    marginBottom: 20,
    '&:hover': {
      backgroundColor: 'inherit',
      color: colors.primaryColor
    }
  },
  media: {
    height: 250
  }
});

const BlogList = ({ classes }) => {
  return (
    <Grid
      container
      spacing={8}
      style={{ paddingLeft: 10, paddingRight: 10 }}
      className={classes.blogContainer}
    >
      {blogList.map(blog => {
        return (
          <Grid item key={blog.id} xs={12} sm={4} md={4} lg={4}>
            <Card elevation={0} className={classes.card}>
              <CardActionArea>
                <CardMedia className={classes.media} image={blog.img} />
                <CardContent>
                  <Typography
                    variant='h5'
                    gutterBottom
                    className={classes.headerTwoStyle}
                  >
                    {blog.title}
                  </Typography>
                  <Typography component='p'>
                    Lizards are a widespread group of squamate reptiles, with
                    over 6,000 species, ranging across all continents except
                    Antarctica
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default withStyles(styles)(BlogList);
