import React from 'react';
import { Link } from 'react-router-dom';
import {
  withStyles,
  Button,
  Avatar,
  Hidden,
  Grid,
  Paper,
  Typography
} from '@material-ui/core';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';
import ClientsPaper from './ClientsPaper';
import ProjectsPaper from './ProjectsPaper';
import BlogList from './BlogList';

const app1 = require('../../images/projects/app1.jpg');
const app2 = require('../../images/projects/app2.jpg');
const app3 = require('../../images/projects/app3.jpg');
const case1 = require('../../images/projects/case1.jpg');
const case2 = require('../../images/projects/case2.jpg');
const case3 = require('../../images/projects/case3.png');
const img1 = require('../../images/bg2.jpg');
const renePhoto = require('../../images/rene.jpg');

const productList = [
  {
    id: 1,
    name: 'NICKSON',
    title: 'ON-DEMAND FURNITURE RENTAL PLATFORM',
    description:
      'An on-demand furniture rental platform, Nickson Living gives people the affordable opportunity to live in a custom-designed, fully-furnished apartment of their dreams.',
    caption: '',
    client: 'Nickson Inc.',
    link: 'nicksonliving.com',
    city: 'Dallas, Texas',
    color: '#020914',
    coverImg: app1,
    category: 'project',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 2,
    name: 'Q.Care',
    title: 'ON-DEMAND BOOKING AND GUIDE',
    description:
      'A trip to Las Vegas is an out of life experience. Plan a trip that includes fine dining in a celebrity chef owned 5 star restaurant, thrills like the zip line, and special attractions.',
    caption: 'Vegaster raised $1.1 Million in 2015.',
    client: 'Facebook Inc',
    link: 'www.facebook.com',
    city: 'San Franscisco',
    color: '#797D7F',
    coverImg: case2,
    category: 'case_study',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 3,
    name: 'The Sports Hub',
    title: 'PLATFORM FOR HOCKEY PLAYERS',
    description:
      'Supporting amateur athletes, The Sports Aux is a platform for hockey players that bridges the gap between rink and stands.',
    caption: '',
    client: 'Google Inc',
    link: 'www.google.com',
    city: 'Palo Alto, CA',
    color: '#000000',
    coverImg: app2,
    category: 'project',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 4,
    name: 'Q.Care',
    title: 'ON-DEMAND PEDIATRIC HELP',
    description:
      'Through PediaQ’s platform, Q.care, hospitals can use a “White Label” smartphone app to offer nurse triage and house calls. The main goal is to reduce unnecessary emergency room visits.',
    caption: 'Q.Care raised $4.5 million in November 2016',
    client: 'Adobe ',
    link: 'www.adobe.com',
    city: 'New york',
    color: '#5DADE2',
    coverImg: case1,
    category: 'case_study',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 5,
    name: 'Swiftmile',
    title: 'BIKE SHARING SYSTEM',
    description:
      'An on-demand eBike sharing system that helps get people out of their cars and where they need to go. The award-winning system is deployed at some of the most innovative companies in the world.',
    caption:
      'Raised $750,000 led by Verizon Ventures. being adopted by Xerox PARC, and more.',
    client: 'HviewTech',
    link: 'www.hviewTech.com',
    city: 'Kigali, Rwanda',
    color: '#273746',
    coverImg: app3,
    category: 'project',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  },
  {
    id: 6,
    name: 'Q.Care',
    title: 'ON-DEMAND BOOKING AND GUIDE',
    description:
      'A trip to Las Vegas is an out of life experience. Plan a trip that includes fine dining in a celebrity chef owned 5 star restaurant, thrills like the zip line, and special attractions.',
    caption: 'Vegaster raised $1.1 Million in 2015.',
    client: 'Sparbee',
    link: 'www.sparbee.com',
    city: 'Chengdu, China',
    color: '#A3E4D7',
    coverImg: case3,
    category: 'case_study',
    photoShot: [
      { id: 1, path: app2 },
      { id: 2, path: app3 },
      { id: 3, path: case1 },
      { id: 4, path: case2 }
    ]
  }
];
const styles = theme => ({
  imgGrid: {
    height: 500,
    borderRadius: 0,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
  },
  paperGrid: {
    display: 'flex',
    flexDirection: 'column',
    height: 500,
    borderRadius: 0,
    paddingLeft: 30,
    // paddingTop: 30,
    paddingRight: 30,
    justifyContent: 'center',
    backgroundColor: 'inherit',
    [theme.breakpoints.down('xs', 'sm', 'md')]: {
      textAlign: 'center',
      justifyContent: 'flex-start',
      paddingTop: 15,
      paddingBottom: 15,
      height: 350
    }
  },
  serviceContainer: {
    backgroundColor: colors.white,
    borderRadius: 5,
    position: 'relative',
    top: -50,
    marginLeft: 60,
    marginRight: 60,
    [theme.breakpoints.down('xs', 'sm')]: {
      marginLeft: 0,
      marginRight: 0,
      top: 0,
      borderRadius: 0
    }
  },
  serviceGrid: {
    display: 'flex',
    backgroundColor: 'inherit',
    height: 250,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    [theme.breakpoints.down('xs', 'sm', 'md')]: {
      height: 'auto'
    }
  },
  serviceRowStyle: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'inherit'
  },
  contactGrid: {
    display: 'flex',
    backgroundColor: colors.gray01,
    height: 200,
    borderRadius: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    [theme.breakpoints.down('xs', 'sm', 'md')]: {
      height: 'auto'
    }
  },
  productImgGrid: {
    height: 350,
    borderRadius: 0,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    [theme.breakpoints.up('xs')]: {
      paddingLeft: 30,
      paddingRight: 30
    }
  },
  productGrid: {
    display: 'flex',
    flexDirection: 'column',
    height: 350,
    borderRadius: 0,
    paddingLeft: 30,
    paddingRight: 30,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.gray01
  },
  bigAvatar: {
    width: 85,
    height: 85
  },
  missionText: {
    color: colors.textWhiteColor,
    fontSize: constants.headerTextTwo,
    fontWeight: '900',
    marginLeft: 10
  },
  missionDescriptionText: {
    color: colors.textWhiteColor,
    fontSize: constants.headerTextFour,
    fontWeight: '300',
    marginLeft: 10
  },
  headerTwoStyle: {
    color: colors.textColor,
    fontSize: constants.headerTextFour,
    fontWeight: '700',
    marginTop: 15
  },
  descriptionTextStyle: {
    color: colors.textColor,
    fontWeight: '300',
    margin: 30
  },
  profileName: {
    color: colors.textColor,
    fontSize: constants.headerTextThree,
    fontWeight: 'bold',
    margin: 0,
    padding: 0
  },
  profileInfo: {
    color: colors.textColor,
    fontSize: 16,
    fontWeight: '400',
    margin: 0,
    padding: 0
  },
  buttonOutLine: {
    borderColor: colors.primaryColor,
    borderRadius: 0,
    color: colors.primaryColor,
    backgroundColor: 'inherit',
    fontSize: 15,
    fontWeight: '600',
    width: 250,
    marginBottom: 30,
    '&:hover': {
      borderColor: colors.primaryColor,
      backgroundColor: colors.primaryColor,
      color: colors.white
    }
  },
  buttonStyle: {
    borderColor: colors.secondary,
    borderRadius: 0,
    color: colors.textColor,
    backgroundColor: colors.secondary,
    fontSize: 15,
    fontWeight: '600',
    width: 250,
    marginTop: 30,
    marginBottom: 30,
    '&:hover': {
      borderColor: colors.secondary,
      backgroundColor: 'inherit',
      color: colors.textColor
    }
  },
  buttonText: {
    color: colors.primaryColor,
    fontSize: constants.subHeaderText,
    fontWeight: 'bold',
    borderColor: 'none',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: 'inherit',
      color: colors.textColor
    }
  }
});
const Home = ({ classes }) => {
  const proj1 = productList.find(prod => (prod = prod.id === 2));
  const CommentCard = () => (
    <Paper elevation={0} className={classes.productGrid}>
      <Avatar alt='Rene La' src={renePhoto} className={classes.bigAvatar} />
      <Typography variant='title' className={classes.profileName}>
        Rene
      </Typography>
      <Typography variant='subtitle2' className={classes.profileInfo}>
        CEO <br />
        LaSoft Systems Inc.
      </Typography>
      <Typography
        variant='subtitle1'
        gutterBottom
        className={classes.descriptionTextStyle}
      >
        LaSoft Systems has consistently delivered on all of nugs.net’s mobile
        projects and has helped us develop two thriving subscription businesses
      </Typography>
    </Paper>
  );
  const renderProduct1 = () => (
    <Grid
      container
      style={{ backgroundColor: colors.gray01, borderRadius: 10 }}
      className={classes.serviceContainer}
      spacing={0}
    >
      <Grid
        component={Link}
        to={{
          pathname: `our_work/${proj1.name.toLowerCase()}`,
          // search: "?sort=name",
          // hash: "#the-hash",
          state: { singleProj: proj1 }
        }}
        item
        xs={12}
        sm={12}
        md={8}
        lg={8}
      >
        <Paper
          elevation={0}
          style={{ backgroundImage: `url(${proj1.coverImg})` }}
          className={classes.productImgGrid}
        />
      </Grid>

      <Grid item md={4} lg={4}>
        <Hidden only={['xs', 'sm']}>{CommentCard()}</Hidden>
      </Grid>

      {/* list of projects */}

      {productList.map(prod => {
        return (
          <Grid
            component={Link}
            to={{
              pathname: `our_work/${prod.name.toLowerCase()}`,
              // search: "?sort=name",
              // hash: "#the-hash",
              state: { singleProj: prod }
            }}
            item
            key={prod.id}
            style={{ padding: 2 }}
            xs={12}
            sm={4}
            md={4}
            lg={4}
          >
            <Paper
              elevation={0}
              className={classes.productImgGrid}
              style={{
                height: 200,
                backgroundImage: `url(${prod.coverImg})`
              }}
            />
          </Grid>
        );
      })}

      <Grid item xs={12}>
        <Paper
          elevation={0}
          style={{ height: 100 }}
          className={classes.contactGrid}
        >
          <Button
            component={Link}
            to={'/our_work'}
            color='primary'
            className={classes.buttonText}
          >
            See More of Our Work
          </Button>
        </Paper>
      </Grid>
    </Grid>
  );

  return (
    <div>
      <Grid container spacing={0}>
        {/* carousel content */}
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Paper
            elevation={0}
            className={classes.imgGrid}
            style={{ backgroundImage: `url(${img1})` }}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Paper elevation={0} className={classes.paperGrid}>
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
            >
              BETTER MOBILE AND WEB EXPERIENCES THAT TRANSFORM YOUR STARTUP
              IDEAS INTO SUCCESSFUL ENTERPRISE
            </Typography>
            <Typography
              variant='subtitle1'
              gutterBottom
              style={{ marginTop: 30 }}
              className={classes.missionDescriptionText}
            >
              We help Businesses with their digital transformation, Building
              beautiful digital products engineered to drive their growth.
            </Typography>
          </Paper>
        </Grid>

        {/* service packages grid */}
        <Grid container className={classes.serviceContainer} spacing={0}>
          <Grid item xs={12}>
            <Paper elevation={0} className={classes.serviceGrid}>
              <Typography
                variant='h4'
                gutterBottom
                style={{ color: colors.textColor }}
                className={classes.missionText}
              >
                Service Packages
              </Typography>
              <Typography
                variant='subtitle1'
                gutterBottom
                style={{ color: colors.textColor }}
                className={classes.missionDescriptionText}
              >
                Our Consultancy packages are the culmination of our expertise
                and proficiency. <br /> Crafted from good product taste, a deep
                technological background and over a <br /> decade of
                experience,they are designed to fulfill your product and
                business goals.
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
            <Paper elevation={0} className={classes.serviceRowStyle}>
              <Typography
                variant='h5'
                gutterBottom
                className={classes.headerTwoStyle}
              >
                Product Design
              </Typography>
              <Typography
                variant='subtitle1'
                gutterBottom
                className={classes.descriptionTextStyle}
              >
                Eye-catching & opinionated design, with meaning behind every
                flourish. Nurtured ground-up with UX in mind, we ensure
                purposeful design for your product, engineered with research
                throughout. Our good taste helps, too.
              </Typography>
              <Button
                variant='outlined'
                color='primary'
                className={classes.buttonOutLine}
              >
                Learn More
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
            <Paper elevation={0} className={classes.serviceRowStyle}>
              <Typography
                variant='h5'
                gutterBottom
                className={classes.headerTwoStyle}
              >
                MVP Development
              </Typography>

              <Typography
                variant='subtitle1'
                gutterBottom
                className={classes.descriptionTextStyle}
              >
                Grow a scalable product, alongside full technological support.
                We’ll help you to validate & understand your product’s value &
                approach, while efficiently taking full advantage of the MVP
                model – from hypothesis, to market, & beyond.
              </Typography>

              <Button
                variant='outlined'
                color='primary'
                className={classes.buttonOutLine}
              >
                Learn More
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
            <Paper elevation={0} className={classes.serviceRowStyle}>
              <Typography
                variant='h5'
                gutterBottom
                className={classes.headerTwoStyle}
              >
                Continuous Product Development
              </Typography>
              <Typography
                variant='subtitle1'
                gutterBottom
                className={classes.descriptionTextStyle}
              >
                Products must be improved post-launch, the key being a long-term
                roadmap. Using analytics & user feedback to help you determine
                this strategy, we’ll implement it via continuous updates – with
                data behind every decision.
              </Typography>

              <Button
                variant='outlined'
                color='primary'
                className={classes.buttonOutLine}
              >
                Learn More
              </Button>
            </Paper>
          </Grid>
        </Grid>

        {/* PortFolio Grid */}
        <Grid item xs={12}>
          <Paper
            elevation={0}
            style={{ backgroundColor: colors.bgColor, height: 150 }}
            className={classes.contactGrid}
          >
            <Typography
              variant='h4'
              gutterBottom
              // style={{ color: colors.textColor }}
              className={classes.missionText}
            >
              Browse By Portfolio
            </Typography>
          </Paper>
        </Grid>
        {renderProduct1()}

        {/* Our clients Grid */}
        <Grid item xs={12}>
          <Paper
            elevation={0}
            style={{ backgroundColor: colors.bgColor, height: 150 }}
            className={classes.contactGrid}
          >
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
            >
              Our Client Partners
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <ClientsPaper />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Hidden only={['xs', 'sm']}>
            <ProjectsPaper />
          </Hidden>
        </Grid>

        {/* our blogs */}
        <Grid item xs={12} style={{}}>
          <Paper
            elevation={0}
            style={{ backgroundColor: colors.bgColor, height: 150 }}
            className={classes.contactGrid}
          >
            <Typography
              variant='h4'
              gutterBottom
              className={classes.missionText}
            >
              Featured Insights from LaSoft Systems
            </Typography>
          </Paper>
        </Grid>
        <BlogList />
        {/* contact us Grid */}
        <Grid item xs={12} style={{ marginTop: 20 }}>
          <Paper elevation={0} className={classes.contactGrid}>
            <Typography
              variant='h4'
              gutterBottom
              style={{ color: colors.textColor }}
              className={classes.missionText}
            >
              Are you thinking about a project ?
            </Typography>
            <Button
              variant='outlined'
              component={Link}
              to={'/contact_us'}
              color='primary'
              className={classes.buttonStyle}
            >
              Contact Us
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Home);
