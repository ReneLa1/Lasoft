import React from 'react';
import { Link } from 'react-router-dom';
import {
  withStyles,
  Typography,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Button,
  Paper,
  Grid,
  Toolbar,
  Avatar,
  GridListTile,
  GridList
} from '@material-ui/core';
import colors from '../../styles/colors';
import * as constants from '../../styles/constants';

const cover = require('../../images/koln.jpg');
const auth1 = require('../../images/rene1.jpg');
const auth2 = require('../../images/sandy.jpg');
const auth3 = require('../../images/cy.jpeg');
const posts = [
  {
    id: 1,
    title: '10 Internet of Things Apps',
    caption: 'Spending on IoT is expected..',
    body:
      'Spending on IoT is expected to top 1 Billion USD by 2022 according to IDC projection',
    authorName: 'Rene La',
    authorRole: 'CEO & Founder',
    authorImg: auth1,
    coverImg: cover,
    reads: 300,
    likes: 200
  },
  {
    id: 2,
    title: '10 Internet of Things Apps',
    caption: 'Spending on IoT is expected to..',
    body:
      'Spending on IoT is expected to top 1 Billion USD by 2022 according to IDC projection',
    authorName: 'Sandy La',
    authorRole: 'Marketing Director',
    authorImg: auth2,
    coverImg: cover,
    reads: 347,
    likes: 250
  },
  {
    id: 3,
    title: '10 Internet of Things Apps',
    caption: 'Spending on IoT is expected to...',
    body:
      'Spending on IoT is expected to top 1 Billion USD by 2022 according to IDC projection',
    authorName: 'Pazzo Cy',
    authorRole: 'Senior Engineer',
    authorImg: auth3,
    coverImg: cover,
    reads: 3000,
    likes: 1020
  }
  // {
  //   id: 4,
  //   title: '10 Internet of Things Apps',
  //   caption: 'Spending on IoT is expected to...',
  //   body:
  //     'Spending on IoT is expected to top 1 Billion USD by 2022 according to IDC projection',
  //   authorName: 'Rene La',
  //   authorRole: 'CEO & Founder',
  //   authorImg: auth4,
  //   coverImg: cover,
  //   reads: 300,
  //   likes: 200
  // }
  // {
  //   id: 5,
  //   title: '10 Internet of Things Apps',
  //   caption: 'Spending on IoT is expected too..',
  //   body:
  //     'Spending on IoT is expected to top 1 Billion USD by 2022 according to IDC projection',
  //   authorName: 'Rene La',
  //   authorRole: 'CEO & Founder',
  //   authorImg: '',
  //   coverImg: cover,
  //   reads: 300,
  //   likes: 200
  // }
];

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 2,
    borderRadius: 0,

    backgroundColor: colors.white
    // height: 200,
    // color: theme.palette.text.secondary
  },
  toolbar: theme.mixins.toolbar,
  largerPost: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: theme.spacing.unit * 2,
    marginBottom: 10
  },
  missionText: {
    color: colors.textColor,
    fontSize: constants.headerTextOne,
    fontWeight: '900',
    marginBottom: 30
  },
  missionDescriptionText: {
    color: colors.textColor,
    fontSize: 16,
    fontWeight: '300',
    marginBottom: 30
  },
  buttonOutLine: {
    borderColor: colors.primaryColor,
    borderRadius: 0,
    color: colors.primaryColor,
    backgroundColor: 'inherit',
    fontSize: 15,
    fontWeight: '600',
    width: 250,

    marginBottom: 30,
    '&:hover': {
      borderColor: colors.primaryColor,
      backgroundColor: colors.primaryColor,
      color: colors.white
    }
  },
  featuredPosts: {
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 0,
    backgroundColor: colors.gray02
  },
  postListContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
    // padding: theme.spacing.unit * 2
  },
  card: {
    width: 150,
    height: 180,
    margin: 10,
    // borderRadius: 0,
    borderColor: 'none',
    backgroundColor: colors.white
  },
  buttonText: {
    color: colors.textColor,
    borderColor: 'none',
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: 'inherit',
      color: colors.primaryColor
    }
  },
  headerTwoStyle: {
    color: colors.textColor,
    fontSize: 12,
    fontWeight: '700',
    // marginTop: 10,
    // marginBottom: 10,
    '&:hover': {
      backgroundColor: 'inherit',
      color: colors.primaryColor
    }
  },
  media: {
    height: 80
  },
  navLinks: {
    display: 'flex',
    flexGrow: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 15
  },
  navStyle: {
    marginLeft: 10,
    marginRight: 10,
    color: colors.textColor,
    textDecoration: 'none',
    fontFamily: 'Raleway',
    fontSize: 13,
    fontWeight: 'bold',
    paddingRight: 10,
    paddingLeft: 10,
    '&:hover': {
      color: colors.primaryColor
    }
  },
  //featured posts grid List style
  postListGrid: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: colors.gray02
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
    backgroundColor: 'inherit',
    // height: 450,
    paddingTop: 30,
    paddingLeft: 30,
    paddingRight: 30
    // paddingBottom: 10
  },
  title: {
    color: theme.palette.primary.light
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)'
  },
  //single post Style

  cardStyle: {
    display: 'flex',
    backgroundColor: 'white'
    // height: 400
  },
  postPhoto: {
    height: 300
  },
  authorInfoContainer: {
    display: 'flex',
    position: 'relative',
    left: -16,
    zIndex: 1,
    top: -40,
    width: 200,
    padding: 5,

    flexDirection: 'row',
    justifyContent: 'flex-start',
    // alignItems:'center'
    backgroundColor: colors.white
  },
  authorAvatar: {
    height: 50,
    width: 50
  }
});

const Blog = props => {
  const { classes } = props;

  const renderFeaturedPosts = (
    <div className={classes.postListGrid}>
      <GridList
        cellHeight={500}
        className={classes.gridList}
        spacing={24}
        cols={2.5}
      >
        {posts.map(tile => (
          <GridListTile key={tile.id}>
            <Card elevation={0} className={classes.cardStyle}>
              <CardActionArea>
                <CardMedia
                  className={classes.postPhoto}
                  image={tile.coverImg}
                />
                <CardContent>
                  <div className={classes.authorInfoContainer}>
                    <Avatar
                      alt={tile.authorName}
                      src={tile.authorImg}
                      className={classes.authorAvatar}
                    />
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        marginLeft: 10
                      }}
                    >
                      <Typography
                        variant='h4'
                        gutterBottom
                        className={classes.headerTwoStyle}
                      >
                        {tile.authorName}
                      </Typography>
                      <Typography
                        variant='h4'
                        gutterBottom
                        className={classes.headerTwoStyle}
                      >
                        {tile.authorRole}
                      </Typography>
                    </div>
                  </div>
                  <Typography
                    variant='h4'
                    gutterBottom
                    className={classes.headerTwoStyle}
                  >
                    {tile.title}
                  </Typography>
                  <Typography component='p'>{tile.caption}</Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
  const renderTopPost = (
    <div className={classes.largerPost}>
      <Typography variant='h4' gutterBottom className={classes.missionText}>
        CREATING SIMPLY BETTER <br /> MOBILE AND WEB EXPERIENCES
      </Typography>
      <Typography
        variant='subtitle1'
        gutterBottom
        className={classes.missionDescriptionText}
      >
        We help Businesses with their digital transformation, Building beautiful
        digital products engineered to drive their growth.
      </Typography>
      <Button
        variant='outlined'
        color='primary'
        className={classes.buttonOutLine}
      >
        Read On
      </Button>
    </div>
  );

  const renderPosts = (
    <div className={classes.postListContainer}>
      <Typography
        variant='subtitle1'
        gutterBottom
        className={classes.missionDescriptionText}
        style={{ fontWeight: 'bold', marginBottom: 10, fontSize: 18 }}
      >
        Our Best Insights
      </Typography>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        {posts.map(post => {
          return (
            <Card elevation={0} className={classes.card} key={post.id}>
              <CardActionArea>
                <CardMedia className={classes.media} image={post.coverImg} />
                <CardContent>
                  <Typography
                    variant='h5'
                    gutterBottom
                    className={classes.headerTwoStyle}
                  >
                    {post.title}
                  </Typography>
                  <Typography component='p'>{post.caption}</Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          );
        })}
      </div>
    </div>
  );

  return (
    <div className={classes.root}>
      <div className={classes.toolbar} />
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={6} lg={5}>
          <Paper elevation={0} className={classes.paper}>
            {renderTopPost}
            {renderPosts}
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={7}>
          <Paper elevation={0} className={classes.featuredPosts}>
            <Toolbar style={{ borderColor: '1px solid white' }}>
              <div className={classes.navLinks}>
                <Typography
                  variant='subtitle1'
                  gutterBottom
                  className={classes.navStyle}
                >
                  All
                </Typography>
                <Typography
                  variant='subtitle1'
                  gutterBottom
                  className={classes.navStyle}
                >
                  Business
                </Typography>
                <Typography
                  variant='subtitle1'
                  component={Link}
                  to={'/our_work'}
                  gutterBottom
                  className={classes.navStyle}
                >
                  Technology
                </Typography>
                <Typography
                  variant='subtitle1'
                  component={Link}
                  to={'/about_us'}
                  gutterBottom
                  className={classes.navStyle}
                >
                  Entrepreneurship
                </Typography>
                <Typography
                  variant='subtitle1'
                  component={Link}
                  to={'/blog'}
                  gutterBottom
                  className={classes.navStyle}
                >
                  Finance
                </Typography>
              </div>
            </Toolbar>
            {renderFeaturedPosts}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Blog);
